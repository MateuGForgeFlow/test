# Copyright (C) 2017-20 ForgeFlow S.L.
# License LGPL-3.0 or later (https://www.gnu.org/licenses/lgpl.html)

{
    "name": "Test",
    "version": "14.0.1.0.0",
    "license": "LGPL-3",
    "category": "RMA",
    "summary": "Testing"
    "in odoo",
    "author": "ForgeFlow",
    "website": "https://github.com/OCA/https://github.com/ForgeFlow/stock-rma",
    # "depends": ["sale"],
    # "demo": ["demo/demo.xml"],
    "data": [
        "security/ir.model.access.csv",
        "views/plants.xml",
        "views/customer.xml",

    ],
    "installable": True,
    "application": True,
}
