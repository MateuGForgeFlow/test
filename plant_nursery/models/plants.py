# Copyright (C) 2017-20 ForgeFlow S.L.
# License LGPL-3.0 or later (https://www.gnu.org/licenses/lgpl.html)

from odoo import fields, models


class Plants(models.Model):
    _name = 'nursery.plant'

    name = fields.Char("name", required=True, default='draft')
    price = fields.Float("price")
    date = fields.Date(
        string='date',
        required=True,
        index=True,
        states={'draft': [('readonly', False)]},
        copy=False,
        default=fields.Date.context_today
    )
    ref = fields.Char(string='ref', copy=False, tracking=True)
    narration = fields.Text(string='narration')
    state = fields.Selection(selection=[
        ('draft', 'Draft'),
        ('posted', 'Posted'),
        ('cancel', 'Cancelled'),
    ], string='state', required=True, copy=False, tracking=True,
        default='draft')
    boolean = fields.Boolean(string ="boolean", help="Technical field for knowing if the move has been posted before", copy=False)

    many2many = fields.Many2many('nursery.customer', string='many2many') #, compute='_compute_suitable_journal_ids')

    one2many = fields.One2many('nursery.customer', 'many2one', string='one2many') ## readonly=True, states={'draft': [('readonly', False)]})
    #
    # amount_tax = fields.Monetary(string='amount')
    #                              # compute='_compute_amount')
    # amount_by_group = fields.Binary(string="Tax amount by group",
    #                                 compute='_compute_invoice_taxes_by_group',
    #                                 help='Edit Tax amounts if you encounter rounding issues.')


